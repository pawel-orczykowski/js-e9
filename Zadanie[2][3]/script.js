const CITY_SPEED_LIMIT = 50;
const TWO_LANE_EXPRESSWAY_SPEED_LIMIT = 120;
const HIGHWAY_SPEED_LIMIT = 140;


function checkSpeed(speed, typeOfRoad ='city'){
console.log("Your speed is "+speed+" km/h\n");

switch(typeOfRoad){
    case ('city'):
        if (speed<=CITY_SPEED_LIMIT){
            console.log("Miasto: jedziesz prawidlowon");
            break;
        }
        else {
            console.log("Miasto: przekroczyles prędkość");
            break;
        }
    case ('expressway'):
        if (speed<=TWO_LANE_EXPRESSWAY_SPEED_LIMIT){
            console.log("Eska: jedziesz prawidlowo");
            break;
        }
        else {
            console.log("Eska: przekroczyles prędkość");
            break;
        }
    case ('highway'):
        if (speed<=HIGHWAY_SPEED_LIMIT){
            console.log("Autostrada: jedziesz prawidlowo");
            break;
        }
        else {
            console.log("Autostrada: przekroczyles prędkość");
            break;
        }
    }
}
checkSpeed(30);
checkSpeed(120, 'highway');
checkSpeed(121, 'expressway')