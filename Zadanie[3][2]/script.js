function returnString (string = "koparka") {
    let stringToReturn = '';

    for (let i = 0; i <string.length; i++){
        if (i % 2 != 0) {
            stringToReturn += string.charAt(i);
        }
    }
    return stringToReturn;
}

