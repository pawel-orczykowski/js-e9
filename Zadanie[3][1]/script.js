function getNumbers(numOne, numTwo){
    let numsAdded = 0;
    for(let i = numOne; i <= numTwo; i++){ 
        numsAdded += i;

        if (i%2==0) {
            console.log(i);
        }
    }
    return "Sum of numbers between " + numOne + " and " + numTwo + " is " + numsAdded;
}